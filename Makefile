NAME		=	base

ASM_SRC		=	start.asm

OUT		=	$(ASM_SRC:.asm=.o)

NASM		=	nasm  

ASM_FLAGS	=	-f elf64

LD		=	ld

LD_FLAGS	=	

RM		=	rm -f

%.o		:	%.asm
			@printf "Compiled % 29s\n " $< | tr ' ' '-'
			@$(NASM) -o $@ $< $(ASM_FLAGS)

all		:	$(NAME)

$(NAME)		:	$(OUT)
			@$(LD) $(LD_FLAGS) -o $(NAME) $(OUT)
			@printf "Built % 32s\n" $(NAME) | tr ' ' '-'

clean		:
			@$(RM) $(OUT)
			@printf "Deleted % 30s\n" $(OUT) | tr ' ' '-'

fclean		:	clean
			@$(RM) $(NAME)
			@printf "Deleted % 30s\n" $(NAME) | tr ' ' '-'

re		:	fclean all

.PHONY:			all clean fclean re
